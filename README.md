# connection-pool

## Getting started

This is a simple project which creates connection pool. We need a database up and running in order to run the project.
This project can be used as a library in other projects in order to create connection pool. 

## Installation

```
mvn clean install

Start the docker db
docker run -d -p 5432:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=password -e POSTGRES_DB=test --name postgres-14.5 postgres:14.5-alpine

Run the Main.java application

```


