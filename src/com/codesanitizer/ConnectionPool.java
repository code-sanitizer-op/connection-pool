package com.codesanitizer;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

public class ConnectionPool implements AutoCloseable{

    private final BlockingQueue<MyConnection> queue;
    private final BlockingQueue<MyConnection> usedConnectionQueue;
    public ConnectionPool(String url, String username, String password, int maxSize){
        queue = new LinkedBlockingDeque<MyConnection>(maxSize);
        usedConnectionQueue = new LinkedBlockingDeque<>(maxSize);

        for(int i=0;i<maxSize;i++){
            queue.offer(createConnection(url, username,password,i));
        }
    }

    private MyConnection createConnection(String url, String username, String password, int id) {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, username, password);

            if (conn != null) {
                System.out.println(Thread.currentThread().getName()+" Connected to the PostgreSQL server successfully.");
            } else {
                System.out.println(Thread.currentThread().getName()+" Failed to make connection!");
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return new MyConnection(conn,id);
    }


    public MyConnection getConnection() throws InterruptedException {
        MyConnection conn = queue.poll(2000, TimeUnit.MILLISECONDS);
        System.out.println(Thread.currentThread().getName()+" polled connection "+conn.getId());
        if(conn == null) {
            //throw new RuntimeException(queue.size()+" connection is not available at the moment");
            System.out.println("no connection");
        }
        usedConnectionQueue.offer(conn);
        return conn;
    }

    public void releaseConnection(MyConnection connection){
        queue.offer(connection);
        usedConnectionQueue.remove(connection);
    }

    @Override
    public void close() throws Exception {
        System.out.println("Auto closing connections");
        while(!usedConnectionQueue.isEmpty()){
            MyConnection conn = usedConnectionQueue.poll();
            conn.getConnection().close();
        }
        while(!queue.isEmpty()){
            MyConnection conn = queue.poll();
            conn.getConnection().close();
        }
        queue.clear();
        usedConnectionQueue.clear();
    }
}