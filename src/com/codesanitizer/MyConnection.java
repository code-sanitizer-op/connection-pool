package com.codesanitizer;

import java.sql.Connection;

public class MyConnection {
    private Connection connection;
    private int id;

    public MyConnection(Connection connection, int id) {
        this.connection = connection;
        this.id = id;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
