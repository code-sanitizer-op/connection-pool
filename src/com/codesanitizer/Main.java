package com.codesanitizer;


public class Main {

    public static void main(String[] args) throws Exception {
        String url = "jdbc:postgresql://localhost/test";
        String user = "postgres";
        String password = "password";
        try (ConnectionPool cp = new ConnectionPool(url,user,password,5)){
            //while(true){
                //MyConnection conn = cp.getConnection();
                //System.out.println(conn.getId()+" "+conn.getConnection());
                Worker thread1 = new Worker(cp);
                Worker thread2 = new Worker(cp);
                thread1.start();
                thread2.start();
           // }
        }
    }

    static class Worker extends Thread {
        private final ConnectionPool cp;
        public Worker(ConnectionPool cp){
            this.cp = cp;
        }

        @Override
        public void run() {
            while(true){
                MyConnection conn = null;
                try {
                    conn = cp.getConnection();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Thread "+Thread.currentThread().getName()+" "+conn.getId()+" "+conn.getConnection());
            }

        }
    }
}
